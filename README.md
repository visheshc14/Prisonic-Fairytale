# Prisonic Fairytale
Prisonic Fairytale - ChronoTrigger's Bad Clone With Massively Multiplayer Online Functionality Built With Godot Engine, Elixir & Docker. Upcoming Blog on Xen About The Making of Prisonic- Fairytale (Yet to Be Deployed). Prisonic Fairytale Name is Derived From Silent Hill - 2 Original Soundtrack - Prisonic Fairytale. [`Apple Music`](https://music.apple.com/gb/album/prisonic-fairytale/164069102?i=164070583).

Few Snapshots -


<img width="787" alt="1" src="https://user-images.githubusercontent.com/36515357/135724570-1c769375-4c39-4867-b537-da1dbb054819.png">

<img width="782" alt="2" src="https://user-images.githubusercontent.com/36515357/135724576-d7c30caf-64d3-4b7f-8128-808fe6a5cbe6.png">

<img width="780" alt="3" src="https://user-images.githubusercontent.com/36515357/135724580-dff3424a-d89a-447b-9413-4d4cf0bd66ae.png">

To Start Your Phoenix Server:

  * Install Dependencies With `mix deps.get`.
  * Create And Migrate Your Database With `mix ecto.setup`.
  * Install Node.js dependencies With `npm install` Inside The `assets` Directory.
  * Start Phoenix Endpoint With `mix phx.server` or / `mix deps.get` & `iex -S mix phx.server`.

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix

## Youtube Reference's Elixir & Godot
- HeartBeast - [Godot Engine 3 - Platform Game Tutorial.](https://www.youtube.com/watch?v=wETY5_9kFtA&list=PL9FzW-m48fn2jlBu_0DRh7PvAt-GULEmd)
- Seb The Cat - [GameDev: Building a 2D multiplayer game! Using Godot and Elixir.](https://www.youtube.com/channel/UCVQ2Rt-GlX3ptQ94GUtulmQ)

